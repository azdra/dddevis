import { Module } from '@nestjs/common';
import { PresentersModule } from './presenters/presenters.module';
import { DatabaseModule } from './infra/database/database.module';

@Module({
  imports: [DatabaseModule, PresentersModule],
})
export class AppModule {}
