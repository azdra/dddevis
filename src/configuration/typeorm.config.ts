import { DataSource } from 'typeorm';
import { MysqlConnectionOptions } from 'typeorm/driver/mysql/MysqlConnectionOptions';
import * as process from 'node:process';

const config: MysqlConnectionOptions = {
  type: 'mysql',
  host: 'db',
  port: parseInt(process.env.MYSQL_PORT) ?? 3306,
  username: process.env.MYSQL_USER,
  password: process.env.MYSQL_PASSWORD,
  database: process.env.MYSQL_DATABASE,
  entities: ['dist/**/*/entities/*.entity{.js,.ts}'],
  // entities: ['dist/entity/**/*{.js,.ts}'],
  // migrations: ['dist/migrations/*{.js,.ts}'],
  // subscribers: ['dist/subscribers/*{.js,.ts}'],
  synchronize: true,
};

export default new DataSource(config);
