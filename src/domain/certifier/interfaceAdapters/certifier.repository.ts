import { DocumentEntity } from '../entities/document.entity';
import { CertifiedDocumentEntity } from '../entities/certified.document.entity';

export interface ICertifierRepository {
  certify(doc: DocumentEntity): Promise<CertifiedDocumentEntity>;
}

export const CERTIFIER_REPOSITORY_TOKEN = 'CERTIFIER_REPOSITORY_TOKEN';
