import { DocumentEntity } from './document.entity';

export interface CertifiedDocumentEntity {
  document: DocumentEntity;
  certificate: string;
}
