import InvoiceEntity from '../../entities/invoice.entity';

export interface IInvoiceRepository {
  createInvoice(invoice: InvoiceEntity): Promise<InvoiceEntity>;
  getInvoice(id: InvoiceEntity['id']): Promise<InvoiceEntity>;
  getInvoices(): Promise<InvoiceEntity[]>;
  updateInvoice(
    id: InvoiceEntity['id'],
    invoice: InvoiceEntity,
  ): Promise<InvoiceEntity>;
  deleteInvoice(id: InvoiceEntity['id']): Promise<unknown>;
}

export const INVOICE_REPOSITORY_TOKEN = 'INVOICE_REPOSITORY_TOKEN';
