import { ReadInvoiceUseCase } from './read-invoice.use-case';
import { IInvoiceRepository } from '../interfaceAdapters/repositories/IInvoiceRepository';
import InvoiceEntity from '../entities/invoice.entity';
import { EInvoiceStatus } from '../enums/EInvoiceStatus';

const mockInvoiceRepository: Partial<IInvoiceRepository> = {
  getInvoice: jest.fn(),
};

describe('ReadInvoiceUseCase', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should return a Invoice if it exists', async () => {
    const existingInvoice: InvoiceEntity = {
      id: 1,
      paymentChoice: 'credit',
      status: EInvoiceStatus.NonAcquitted,
      comment: '',
      createdAt: undefined,
      updatedAt: undefined,
      client: undefined,
      company: undefined,
      lines: [],
      total: 0,
      totalWithoutTax: 0,
    };

    mockInvoiceRepository.getInvoice = jest
      .fn()
      .mockResolvedValue(existingInvoice);

    const readInvoiceUseCase = new ReadInvoiceUseCase(
      mockInvoiceRepository as IInvoiceRepository,
    );

    const Invoice = await readInvoiceUseCase.execute(existingInvoice.id);

    expect(mockInvoiceRepository.getInvoice).toHaveBeenCalledWith(
      existingInvoice.id,
    );
    expect(Invoice).toEqual(existingInvoice);
  });

  it('should throw an error if Invoice does not exist', async () => {
    const nonExistentInvoiceId = 999;

    mockInvoiceRepository.getInvoice = jest.fn().mockResolvedValue(null);

    const readInvoiceUseCase = new ReadInvoiceUseCase(
      mockInvoiceRepository as IInvoiceRepository,
    );

    try {
      await readInvoiceUseCase.execute(nonExistentInvoiceId);
    } catch (error) {
      expect(error).toBeInstanceOf(Error);
    }

    expect(mockInvoiceRepository.getInvoice).toHaveBeenCalledWith(
      nonExistentInvoiceId,
    );
  });
});
