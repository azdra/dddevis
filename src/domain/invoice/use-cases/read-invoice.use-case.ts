import { Inject, Injectable } from '@nestjs/common';
import { IUseCase } from '../../../infra/IUseCase';
import {
  IInvoiceRepository,
  INVOICE_REPOSITORY_TOKEN,
} from '../interfaceAdapters/repositories/IInvoiceRepository';
import InvoiceEntity from '../entities/invoice.entity';

@Injectable()
export class ReadInvoiceUseCase implements IUseCase {
  constructor(
    @Inject(INVOICE_REPOSITORY_TOKEN)
    private invoiceRepository: IInvoiceRepository,
  ) {}

  public execute(id: number): Promise<InvoiceEntity> {
    return this.invoiceRepository.getInvoice(id);
  }
}
