import { Inject, Injectable } from '@nestjs/common';
import { IUseCase } from '../../../infra/IUseCase';
import {
  IInvoiceRepository,
  INVOICE_REPOSITORY_TOKEN,
} from '../interfaceAdapters/repositories/IInvoiceRepository';
import InvoiceEntity from '../entities/invoice.entity';

@Injectable()
export class ReadInvoicesUseCase implements IUseCase {
  constructor(
    @Inject(INVOICE_REPOSITORY_TOKEN)
    private quoteRepository: IInvoiceRepository,
  ) {}

  public execute(): Promise<InvoiceEntity[]> {
    return this.quoteRepository.getInvoices();
  }
}
