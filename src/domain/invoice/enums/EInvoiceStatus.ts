export enum EInvoiceStatus {
  Acquitted = 'acquitté',
  NonAcquitted = 'non acquitté',
}
