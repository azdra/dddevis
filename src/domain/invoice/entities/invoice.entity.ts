import { IInvoiceEntity } from '../interface/IInvoiceEntity';
import { EInvoiceStatus } from '../enums/EInvoiceStatus';
import { Column, Entity, ManyToOne, OneToMany } from 'typeorm';
import DocumentContentEntity from '../../doc-content/entities/documentContent.entity';
import Line from '../../doc-content/line/entities/line.entity';
import LineEntity from '../../doc-content/line/entities/line.entity';
import ContactEntity from '../../doc-content/contact/entities/contact.entity';

@Entity('invoice')
export default class InvoiceEntity
  extends DocumentContentEntity
  implements IInvoiceEntity
{
  @Column()
  public paymentChoice: string;

  @Column()
  public status: EInvoiceStatus;

  @ManyToOne(() => ContactEntity, (contact) => contact.invoice_company, {
    cascade: true,
  })
  public company: ContactEntity;

  @ManyToOne(() => ContactEntity, (contact) => contact.invoice_client, {
    cascade: true,
  })
  public client: ContactEntity;

  @OneToMany(() => Line, (line) => line.document_invoice, { cascade: true })
  public lines: LineEntity[];

  public constructor(data?: IInvoiceEntity) {
    super(data);
    Object.assign(this, data);
  }
}
