import { Module } from '@nestjs/common';
import { InvoiceController } from '../../presenters/controllers/invoice.controller';
import { InvoiceRepositoryImpl } from '../../infra/repositories/sql/invoice.repository.impl';
import { ReadInvoiceUseCase } from './use-cases/read-invoice.use-case';
import { INVOICE_REPOSITORY_TOKEN } from './interfaceAdapters/repositories/IInvoiceRepository';
import { ReadInvoicesUseCase } from './use-cases/read-invoices.use-case';

@Module({
  providers: [
    ReadInvoiceUseCase,
    ReadInvoicesUseCase,
    {
      provide: INVOICE_REPOSITORY_TOKEN,
      useClass: InvoiceRepositoryImpl,
    },
  ],
  exports: [INVOICE_REPOSITORY_TOKEN],
  controllers: [InvoiceController],
})
export class InvoiceModule {}
