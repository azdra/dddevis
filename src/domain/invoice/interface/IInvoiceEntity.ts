import { IDocumentContentEntity } from '../../doc-content/interfaces/IDocumentContentEntity';
import { EInvoiceStatus } from '../enums/EInvoiceStatus';

export interface IInvoiceEntity extends IDocumentContentEntity {
  paymentChoice: string;
  status: EInvoiceStatus;
}
