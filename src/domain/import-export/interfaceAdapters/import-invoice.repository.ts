import InvoiceEntity from '../../invoice/entities/invoice.entity';

export interface IImportInvoiceRepository {
  importInvoice(Invoice: any): Promise<InvoiceEntity>;
  exportInvoice(Invoice: InvoiceEntity): Promise<void>;
}

export const IMPORT_INVOICE_REPOSITORY_TOKEN =
  'IMPORT_INVOICE_REPOSITORY_TOKEN';
