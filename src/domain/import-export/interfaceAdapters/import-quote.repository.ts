import { QuoteEntity } from '../../quote/entities/quote.entity';

export interface IImportQuoteRepository {
  importQuote(quote: any): Promise<QuoteEntity>;
  exportQuote(quote: QuoteEntity): Promise<void>;
}

export const IMPORT_QUOTE_REPOSITORY_TOKEN = 'IMPORT_QUOTE_REPOSITORY_TOKEN';
