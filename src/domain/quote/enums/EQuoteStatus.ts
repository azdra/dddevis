export enum EQuoteStatus {
  Acquitted = 'acquitté',
  NonAcquitted = 'non acquitté',
  Invoiced = 'facturé',
}
