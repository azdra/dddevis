import { QuoteEntity } from '../../entities/quote.entity';

export interface IQuoteRepository {
  createQuote(quote: QuoteEntity): Promise<QuoteEntity>;
  getQuote(id: QuoteEntity['id']): Promise<QuoteEntity>;
  getQuotes(): Promise<QuoteEntity[]>;
  updateQuote(id: QuoteEntity['id'], quote: QuoteEntity): Promise<QuoteEntity>;
  deleteQuote(id: QuoteEntity['id']): Promise<unknown>;
}

export const QUOTE_REPOSITORY_TOKEN = 'QUOTE_REPOSITORY_TOKEN';
