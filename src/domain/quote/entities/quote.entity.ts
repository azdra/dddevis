import DocumentContent from '../../doc-content/entities/documentContent.entity';
import { IQuoteEntity } from '../interface/IQuoteEntity';
import { Column, Entity, ManyToOne, OneToMany } from 'typeorm';
import { EQuoteStatus } from '../enums/EQuoteStatus';
import Line from '../../doc-content/line/entities/line.entity';
import LineEntity from '../../doc-content/line/entities/line.entity';
import ContactEntity from '../../doc-content/contact/entities/contact.entity';

@Entity('quote')
export class QuoteEntity extends DocumentContent implements IQuoteEntity {
  @Column({
    type: 'enum',
    enum: EQuoteStatus,
    default: EQuoteStatus.NonAcquitted,
  })
  public status: EQuoteStatus;

  @OneToMany(() => Line, (line) => line.document_quote, { cascade: true })
  public lines: LineEntity[];

  @ManyToOne(() => ContactEntity, (contact) => contact.quote_company, {
    cascade: true,
  })
  public company: ContactEntity;

  @ManyToOne(() => ContactEntity, (contact) => contact.quote_client, {
    cascade: true,
  })
  public client: ContactEntity;

  public constructor(data?: Partial<IQuoteEntity>) {
    super();
    Object.assign(this, data);
  }

  addLine(line: LineEntity): void {
    this.lines.push(line);
  }
}
