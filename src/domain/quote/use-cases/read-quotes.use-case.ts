import { Inject, Injectable } from '@nestjs/common';
import { QuoteEntity } from '../entities/quote.entity';
import { IUseCase } from '../../../infra/IUseCase';
import {
  IQuoteRepository,
  QUOTE_REPOSITORY_TOKEN,
} from '../interfaceAdapters/repositories/IQuoteRepository';

@Injectable()
export class ReadQuotesUseCase implements IUseCase {
  constructor(
    @Inject(QUOTE_REPOSITORY_TOKEN) private quoteRepository: IQuoteRepository,
  ) {}

  public execute(): Promise<QuoteEntity[]> {
    return this.quoteRepository.getQuotes();
  }
}
