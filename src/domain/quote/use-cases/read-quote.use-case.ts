import { Inject, Injectable } from '@nestjs/common';
import { QuoteEntity } from '../entities/quote.entity';
import { IUseCase } from '../../../infra/IUseCase';
import {
  IQuoteRepository,
  QUOTE_REPOSITORY_TOKEN,
} from '../interfaceAdapters/repositories/IQuoteRepository';

@Injectable()
export class ReadQuoteUseCase implements IUseCase {
  constructor(
    @Inject(QUOTE_REPOSITORY_TOKEN) private quoteRepository: IQuoteRepository,
  ) {}

  public execute(id: number): Promise<QuoteEntity> {
    return this.quoteRepository.getQuote(id);
  }
}
