import { Inject, Injectable } from '@nestjs/common';
import { IUseCase } from '../../../infra/IUseCase';
import { QuoteEntity } from '../entities/quote.entity';
import InvoiceEntity from '../../invoice/entities/invoice.entity';
import { IQuoteMapper, QUOTE_MAPPER_TOKEN } from '../interface/IQuoteMapper';
import {
  IQuoteRepository,
  QUOTE_REPOSITORY_TOKEN,
} from '../interfaceAdapters/repositories/IQuoteRepository';
import {
  IInvoiceRepository,
  INVOICE_REPOSITORY_TOKEN,
} from '../../invoice/interfaceAdapters/repositories/IInvoiceRepository';
import { EQuoteStatus } from '../enums/EQuoteStatus';

@Injectable()
export class TransformToInvoiceUseCase implements IUseCase {
  constructor(
    @Inject(QUOTE_REPOSITORY_TOKEN) private quoteRepository: IQuoteRepository,
    @Inject(INVOICE_REPOSITORY_TOKEN)
    private invoiceRepository: IInvoiceRepository,
    @Inject(QUOTE_MAPPER_TOKEN) private quoteMapper: IQuoteMapper,
  ) {}

  public async execute(quoteId: QuoteEntity['id']): Promise<InvoiceEntity> {
    const quote = await this.quoteRepository.getQuote(quoteId);
    quote.status = EQuoteStatus.Invoiced;

    const lines = quote.lines;

    delete quote.lines;

    await this.quoteRepository.updateQuote(quoteId, quote);

    const invoice = this.quoteMapper.toInvoice(quote);
    invoice.lines = lines;

    return this.invoiceRepository.createInvoice(invoice);
  }
}
