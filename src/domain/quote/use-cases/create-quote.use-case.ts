import { QuoteFactory } from '../factories/quote.factory';
import { Inject, Injectable } from '@nestjs/common';
import CreateQuoteDto from '../../../presenters/dtos/quote/create-quote.dto';
import { QuoteEntity } from '../entities/quote.entity';
import { IUseCase } from '../../../infra/IUseCase';
import { QUOTE_FACTORY_TOKEN } from '../interface/IQuoteFactory';
import {
  IQuoteRepository,
  QUOTE_REPOSITORY_TOKEN,
} from '../interfaceAdapters/repositories/IQuoteRepository';

@Injectable()
export class CreateQuoteUseCase implements IUseCase {
  constructor(
    @Inject(QUOTE_REPOSITORY_TOKEN) private quoteRepository: IQuoteRepository,
    @Inject(QUOTE_FACTORY_TOKEN) private quoteFactory: QuoteFactory,
  ) {}

  public async execute(quoteDTO: CreateQuoteDto): Promise<QuoteEntity> {
    const quote = await this.quoteFactory.createQuote(quoteDTO);
    return this.quoteRepository.createQuote(quote);
  }
}
