import { ReadQuoteUseCase } from './read-quote.use-case';
import { IQuoteRepository } from '../interfaceAdapters/repositories/IQuoteRepository';
import { QuoteEntity } from '../entities/quote.entity';
import { EQuoteStatus } from '../enums/EQuoteStatus';

const mockQuoteRepository: Partial<IQuoteRepository> = {
  getQuote: jest.fn(),
};

describe('ReadQuoteUseCase', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should return a quote if it exists', async () => {
    const existingQuote: QuoteEntity = {
      id: 1,
      status: EQuoteStatus.NonAcquitted,
      comment: '',
      createdAt: undefined,
      updatedAt: undefined,
      client: undefined,
      company: undefined,
      lines: [],
      total: 0,
      totalWithoutTax: 0,
      addLine: function (): void {
        throw new Error('Function not implemented.');
      },
    };

    mockQuoteRepository.getQuote = jest.fn().mockResolvedValue(existingQuote);

    const readQuoteUseCase = new ReadQuoteUseCase(
      mockQuoteRepository as IQuoteRepository,
    );

    const quote = await readQuoteUseCase.execute(existingQuote.id);

    expect(mockQuoteRepository.getQuote).toHaveBeenCalledWith(existingQuote.id);
    expect(quote).toEqual(existingQuote);
  });

  it('should throw an error if quote does not exist', async () => {
    const nonExistentQuoteId = 999;

    mockQuoteRepository.getQuote = jest.fn().mockResolvedValue(null);

    const readQuoteUseCase = new ReadQuoteUseCase(
      mockQuoteRepository as IQuoteRepository,
    );

    try {
      await readQuoteUseCase.execute(nonExistentQuoteId);
    } catch (error) {
      expect(error).toBeInstanceOf(Error);
    }

    expect(mockQuoteRepository.getQuote).toHaveBeenCalledWith(
      nonExistentQuoteId,
    );
  });
});
