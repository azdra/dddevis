import { DeleteQuoteUseCase } from './delete-quote.use-case';
import { IQuoteRepository } from '../interfaceAdapters/repositories/IQuoteRepository';
import { QuoteEntity } from '../entities/quote.entity';
import { EQuoteStatus } from '../enums/EQuoteStatus';

const mockQuoteRepository: Partial<IQuoteRepository> = {
  deleteQuote: jest.fn(),
  getQuote: jest.fn(),
};

describe('DeleteQuoteUseCase', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should delete a quote', async () => {
    const quoteToDelete: QuoteEntity = {
      id: 1,
      status: EQuoteStatus.NonAcquitted,
      comment: '',
      createdAt: undefined,
      updatedAt: undefined,
      client: undefined,
      company: undefined,
      lines: [],
      total: 0,
      totalWithoutTax: 0,
      addLine: function (): void {
        throw new Error('Function not implemented.');
      },
    };

    mockQuoteRepository.getQuote = jest.fn().mockReturnValue(quoteToDelete);

    const deleteQuoteUseCase = new DeleteQuoteUseCase(
      mockQuoteRepository as IQuoteRepository,
    );

    await deleteQuoteUseCase.execute(quoteToDelete.id);

    expect(mockQuoteRepository.deleteQuote).toHaveBeenCalledWith(
      quoteToDelete.id,
    );

    // check if the quote was deleted
    mockQuoteRepository.getQuote = jest.fn().mockReturnValue(null);
    const deletedQuote = await mockQuoteRepository.getQuote(quoteToDelete.id);
    expect(deletedQuote).toBeNull();
  });

  it('should throw an error if quote to delete is not found', async () => {
    const quoteId = 5;

    mockQuoteRepository.getQuote = jest.fn().mockReturnValue(null);

    const deleteQuoteUseCase = new DeleteQuoteUseCase(
      mockQuoteRepository as IQuoteRepository,
    );

    try {
      await deleteQuoteUseCase.execute(quoteId);
    } catch (error) {
      expect(error).toBeInstanceOf(Error);
    }
  });
});
