import { CreateQuoteUseCase } from './create-quote.use-case';
import { IQuoteRepository } from '../interfaceAdapters/repositories/IQuoteRepository';
import { QuoteFactory } from '../factories/quote.factory';
import CreateQuoteDto from '../../../presenters/dtos/quote/create-quote.dto';
import { QuoteEntity } from '../entities/quote.entity';
import Contact from '../../doc-content/contact/entities/contact.entity';
import CreateLineDto from 'src/presenters/dtos/line/create-line.dto';
import Line from '../../doc-content/line/entities/line.entity';
import { AddressEntity } from '../../doc-content/address/entities/address.entity';
import { EQuoteStatus } from '../enums/EQuoteStatus';

const mockQuoteRepository: Partial<IQuoteRepository> = {
  createQuote: jest.fn(),
};

const mockQuoteFactory: Partial<QuoteFactory> = {
  createQuote: jest.fn(),
};

describe('CreateQuoteUseCase', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should create a quote', async () => {
    const companyAddress: AddressEntity = {
      id: 1,
      country: 'France',
      street: '6 rue Irvoy',
      city: 'Grenoble',
      zipCode: '38160',
      createdAt: new Date(),
      updatedAt: new Date(),
      contacts: [],
    };

    const companyContact: Contact = {
      id: 1,
      firstName: 'John',
      lastName: 'Doe',
      mail: 'johndoe@esgi.com',
      phone: '0476469059',
      socialReason: 'SAS ESGI',
      address: companyAddress,
      createdAt: new Date(),
      updatedAt: new Date(),
      quote_client: [],
      quote_company: [],
      invoice_client: [],
      invoice_company: [],
    };

    const clientAddress: AddressEntity = {
      id: 2,
      country: 'France',
      street: '25 rue de la république',
      city: 'Grenoble',
      zipCode: '38160',
      createdAt: new Date(),
      updatedAt: new Date(),
      contacts: [],
    };

    const clientContact: Contact = {
      id: 2,
      firstName: 'Jane',
      lastName: 'Doe',
      mail: 'janedoe@revoir.com',
      phone: '0496659049',
      socialReason: 'SAS REVOIR',
      address: clientAddress,
      createdAt: new Date(),
      updatedAt: new Date(),
      quote_client: [],
      quote_company: [],
      invoice_client: [],
      invoice_company: [],
    };

    const quoteLinesDto: CreateLineDto[] = [
      {
        description: 'Consulting',
        unitPrice: 500,
        quantity: 1,
        unit: 'hour',
        tax: 20,
      },
      {
        description: 'Development',
        unitPrice: 1000,
        quantity: 2,
        unit: 'hour',
        tax: 20,
      },
    ];

    const quoteLines: Line[] = [
      {
        id: 1,
        description: 'Consulting',
        unitPrice: 500,
        quantity: 1,
        unit: 'hour',
        tax: 20,
        createdAt: undefined,
        updatedAt: undefined,
        total: 600,
        totalWithoutTax: 500,
        document_quote: undefined,
        document_invoice: undefined,
      },
      {
        id: 2,
        description: 'Development',
        unitPrice: 1000,
        quantity: 2,
        unit: 'hour',
        tax: 20,
        createdAt: undefined,
        updatedAt: undefined,
        document_quote: undefined,
        document_invoice: undefined,
        total: 2400,
        totalWithoutTax: 2000,
      },
    ];

    const createQuoteDto: CreateQuoteDto = {
      comment: "Devis valable jusqu'au 29/08/2024",
      client: clientContact,
      lines: quoteLinesDto,
    };

    const createdQuote: QuoteEntity = {
      id: 1,
      company: companyContact,
      comment: "Devis valable jusqu'au 29/08/2024",
      client: clientContact,
      status: EQuoteStatus.NonAcquitted,
      createdAt: new Date(),
      updatedAt: new Date(),
      lines: quoteLines,
      total: 3000,
      totalWithoutTax: 2500,
      addLine: function (): void {
        throw new Error('Function not implemented.');
      },
    };

    mockQuoteFactory.createQuote = jest.fn().mockReturnValue(createdQuote);
    mockQuoteRepository.createQuote = jest.fn().mockReturnValue(createdQuote);

    const createQuoteUseCase = new CreateQuoteUseCase(
      mockQuoteRepository as IQuoteRepository,
      mockQuoteFactory as QuoteFactory,
    );

    const result = await createQuoteUseCase.execute(createQuoteDto);

    expect(mockQuoteFactory.createQuote).toHaveBeenCalledWith(createQuoteDto);
    expect(mockQuoteRepository.createQuote).toHaveBeenCalledWith(createdQuote);

    expect(result).toEqual(createdQuote);
  });
});
