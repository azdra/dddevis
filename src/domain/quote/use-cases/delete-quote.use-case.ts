import { Inject, Injectable } from '@nestjs/common';
import { IUseCase } from '../../../infra/IUseCase';
import {
  IQuoteRepository,
  QUOTE_REPOSITORY_TOKEN,
} from '../interfaceAdapters/repositories/IQuoteRepository';

@Injectable()
export class DeleteQuoteUseCase implements IUseCase {
  constructor(
    @Inject(QUOTE_REPOSITORY_TOKEN) private quoteRepository: IQuoteRepository,
  ) {}

  public execute(quoteId: number): Promise<unknown> {
    return this.quoteRepository.deleteQuote(quoteId);
  }
}
