import { Inject, Injectable } from '@nestjs/common';
import { QuoteEntity } from '../entities/quote.entity';
import { IUseCase } from '../../../infra/IUseCase';
import {
  IQuoteRepository,
  QUOTE_REPOSITORY_TOKEN,
} from '../interfaceAdapters/repositories/IQuoteRepository';
import CreateLineDto from '../../../presenters/dtos/line/create-line.dto';
import LineEntity from '../../doc-content/line/entities/line.entity';
import {
  ILineRepository,
  LINE_REPOSITORY_TOKEN,
} from '../../doc-content/line/interfaceAdapters/repositories/ILineRepository';

@Injectable()
export class AddLineToQuoteUseCase implements IUseCase {
  constructor(
    @Inject(QUOTE_REPOSITORY_TOKEN) private quoteRepository: IQuoteRepository,
    @Inject(LINE_REPOSITORY_TOKEN) private lineRepository: ILineRepository,
  ) {}

  public async execute(
    line: CreateLineDto,
    quoteId: QuoteEntity['id'],
  ): Promise<LineEntity> {
    const newLine = new LineEntity({
      ...line,
      createdAt: new Date(),
      updatedAt: new Date(),
    });
    newLine.document_quote = await this.quoteRepository.getQuote(quoteId);
    return this.lineRepository.createLine(newLine);
  }
}
