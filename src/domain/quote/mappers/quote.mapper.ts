import { Injectable } from '@nestjs/common';
import { QuoteEntity } from '../entities/quote.entity';
import InvoiceEntity from '../../invoice/entities/invoice.entity';
import { EInvoiceStatus } from '../../invoice/enums/EInvoiceStatus';
import { IQuoteMapper } from '../interface/IQuoteMapper';

@Injectable()
export class QuoteMapper implements IQuoteMapper {
  public toInvoice(quote: QuoteEntity): InvoiceEntity {
    return new InvoiceEntity({
      ...quote,
      paymentChoice: '',
      status: EInvoiceStatus.NonAcquitted,
    });
  }
}
