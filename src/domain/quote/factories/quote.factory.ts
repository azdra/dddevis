import CreateQuoteDto from '../../../presenters/dtos/quote/create-quote.dto';
import { QuoteEntity } from '../entities/quote.entity';
import { Injectable } from '@nestjs/common';
import { IQuoteFactory } from '../interface/IQuoteFactory';

@Injectable()
export class QuoteFactory implements IQuoteFactory {
  public async createQuote(quoteDto: CreateQuoteDto): Promise<QuoteEntity> {
    return new QuoteEntity({
      ...quoteDto,
    });
  }
}
