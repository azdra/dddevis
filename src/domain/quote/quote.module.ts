import { Module } from '@nestjs/common';
import { QuoteController } from '../../presenters/controllers/quote.controller';
import { QUOTE_FACTORY_TOKEN } from './interface/IQuoteFactory';
import { QuoteFactory } from './factories/quote.factory';
import { QuoteRepositoryImpl } from '../../infra/repositories/sql/quote.repository.impl';
import { QuoteMapper } from './mappers/quote.mapper';
import { QUOTE_MAPPER_TOKEN } from './interface/IQuoteMapper';
import { ReadQuoteUseCase } from './use-cases/read-quote.use-case';
import { CreateQuoteUseCase } from './use-cases/create-quote.use-case';
import { DeleteQuoteUseCase } from './use-cases/delete-quote.use-case';
import { TransformToInvoiceUseCase } from './use-cases/transform-to-invoice.use-case';
import { QUOTE_REPOSITORY_TOKEN } from './interfaceAdapters/repositories/IQuoteRepository';
import { INVOICE_REPOSITORY_TOKEN } from '../invoice/interfaceAdapters/repositories/IInvoiceRepository';
import { InvoiceRepositoryImpl } from '../../infra/repositories/sql/invoice.repository.impl';
import { ReadQuotesUseCase } from './use-cases/read-quotes.use-case';
import { AddLineToQuoteUseCase } from './use-cases/add-line-to-quote.use-case';
import { CONTACT_REPOSITORY_TOKEN } from '../doc-content/contact/interfaceAdapters/repositories/IContactRepository';
import { ContactRepositoryImpl } from '../../infra/repositories/sql/contact.repository.impl';
import { LINE_REPOSITORY_TOKEN } from '../doc-content/line/interfaceAdapters/repositories/ILineRepository';
import { LineRepositoryImpl } from '../../infra/repositories/sql/line.repository.impl';

@Module({
  controllers: [QuoteController],
  providers: [
    ReadQuoteUseCase,
    ReadQuotesUseCase,
    CreateQuoteUseCase,
    AddLineToQuoteUseCase,
    DeleteQuoteUseCase,
    TransformToInvoiceUseCase,
    {
      provide: QUOTE_REPOSITORY_TOKEN,
      useClass: QuoteRepositoryImpl,
    },
    {
      provide: INVOICE_REPOSITORY_TOKEN,
      useClass: InvoiceRepositoryImpl,
    },
    {
      provide: LINE_REPOSITORY_TOKEN,
      useClass: LineRepositoryImpl,
    },
    {
      provide: QUOTE_FACTORY_TOKEN,
      useClass: QuoteFactory,
    },
    {
      provide: QUOTE_MAPPER_TOKEN,
      useClass: QuoteMapper,
    },
    {
      provide: CONTACT_REPOSITORY_TOKEN,
      useClass: ContactRepositoryImpl,
    },
  ],
  exports: [QUOTE_REPOSITORY_TOKEN],
})
export class QuoteModule {}
