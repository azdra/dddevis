import { IDocumentContentEntity } from '../../doc-content/interfaces/IDocumentContentEntity';
import { EQuoteStatus } from '../enums/EQuoteStatus';

export interface IQuoteEntity extends IDocumentContentEntity {
  status: EQuoteStatus;
}
