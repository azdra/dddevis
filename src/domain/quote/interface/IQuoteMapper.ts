import { QuoteEntity } from '../entities/quote.entity';
import InvoiceEntity from '../../invoice/entities/invoice.entity';

export interface IQuoteMapper {
  toInvoice(quote: QuoteEntity): InvoiceEntity;
}

export const QUOTE_MAPPER_TOKEN = 'QUOTE_MAPPER_TOKEN';
