import CreateQuoteDto from '../../../presenters/dtos/quote/create-quote.dto';
import { QuoteEntity } from '../entities/quote.entity';

export interface IQuoteFactory {
  createQuote(quoteDto: CreateQuoteDto): Promise<QuoteEntity>;
}

export const QUOTE_FACTORY_TOKEN = 'QUOTE_FACTORY_TOKEN';
