import { ILine } from '../interface/ILine';
import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { QuoteEntity } from '../../../quote/entities/quote.entity';
import InvoiceEntity from '../../../invoice/entities/invoice.entity';

@Entity('line')
export default class LineEntity implements ILine {
  @PrimaryGeneratedColumn()
  public id: number;

  @Column()
  public description: string;

  @Column()
  public quantity: number;

  @Column()
  public tax: number;

  @Column()
  public unit: string;

  @Column()
  public unitPrice: number;

  @CreateDateColumn()
  public createdAt: Date;

  @UpdateDateColumn()
  public updatedAt: Date;

  @ManyToOne(() => QuoteEntity, (document) => document.lines)
  public document_quote: QuoteEntity;

  @ManyToOne(() => InvoiceEntity, (document) => document.lines)
  public document_invoice: InvoiceEntity;

  public constructor(data?: Partial<ILine>) {
    Object.assign(this, data);
  }

  public get total(): number {
    return this.unitPrice * this.quantity * (1 + this.tax / 100);
  }

  public get totalWithoutTax(): number {
    return this.total / (1 + this.tax / 100);
  }
}
