import LineEntity from '../../entities/line.entity';

export interface ILineRepository {
  createLine(Line: LineEntity): Promise<LineEntity>;
  getLine(id: LineEntity['id']): Promise<LineEntity>;
  getLines(): Promise<LineEntity[]>;
  updateLine(id: LineEntity['id'], Line: LineEntity): Promise<LineEntity>;
  deleteLine(id: LineEntity['id']): Promise<unknown>;
}

export const LINE_REPOSITORY_TOKEN = 'LINE_REPOSITORY_TOKEN';
