export interface ILine {
  id?: number;
  description: string;
  unitPrice: number;
  quantity: number;
  unit: string;
  tax: number;
  createdAt: Date;
  updatedAt: Date;
}
