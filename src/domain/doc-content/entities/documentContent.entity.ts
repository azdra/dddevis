import Line from '../line/entities/line.entity';
import { IDocumentContentEntity } from '../interfaces/IDocumentContentEntity';
import {
  Column,
  CreateDateColumn,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import ContactEntity from '../contact/entities/contact.entity';

export default abstract class DocumentContentEntity
  implements IDocumentContentEntity
{
  @PrimaryGeneratedColumn()
  public id: number;

  @Column()
  public comment: string;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  protected company: ContactEntity;
  protected client: ContactEntity;
  protected lines: Line[];

  protected constructor(data?: IDocumentContentEntity) {
    Object.assign(this, data);
  }

  public get total(): number {
    return this.lines.reduce((acc, line) => acc + line.total, 0);
  }

  public get totalWithoutTax(): number {
    return this.lines.reduce((acc, line) => acc + line.totalWithoutTax, 0);
  }
}
