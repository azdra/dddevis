import { IAddressEntity } from '../interfaces/IAddressEntity';
import {
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import ContactEntity from '../../contact/entities/contact.entity';

@Entity('address')
export class AddressEntity implements IAddressEntity {
  @PrimaryGeneratedColumn()
  public id: number;

  @Column()
  public city: string;

  @Column()
  public country: string;

  @Column()
  public street: string;

  @Column()
  public zipCode: string;

  @CreateDateColumn()
  public createdAt: Date;

  @UpdateDateColumn()
  public updatedAt: Date;

  @OneToMany(() => ContactEntity, (photo) => photo.address)
  public contacts: ContactEntity[];

  public constructor(data: IAddressEntity) {
    Object.assign(this, data);
  }
}
