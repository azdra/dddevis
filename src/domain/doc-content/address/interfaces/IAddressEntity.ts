export interface IAddressEntity {
  id?: number;
  street: string;
  city: string;
  zipCode: string;
  country: string;
  createdAt: Date;
  updatedAt: Date;
}
