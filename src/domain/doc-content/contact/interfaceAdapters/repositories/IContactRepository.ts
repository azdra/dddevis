import { UpdateResult } from 'typeorm';
import ContactEntity from '../../entities/contact.entity';

export interface IContactRepository {
  createContact(invoice: ContactEntity): Promise<ContactEntity>;
  getContact(id: ContactEntity['id']): Promise<ContactEntity>;
  getContacts(): Promise<ContactEntity[]>;
  updateContact(
    id: ContactEntity['id'],
    invoice: ContactEntity,
  ): Promise<UpdateResult>;
  deleteContact(id: ContactEntity['id']): Promise<unknown>;
}

export const CONTACT_REPOSITORY_TOKEN = 'CONTACT_REPOSITORY_TOKEN';
