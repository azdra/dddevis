import { IContactEntity } from '../interfaces/IContactEntity';
import { AddressEntity } from '../../address/entities/address.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { QuoteEntity } from '../../../quote/entities/quote.entity';
import InvoiceEntity from '../../../invoice/entities/invoice.entity';

@Entity('contact')
export default class ContactEntity implements IContactEntity {
  @PrimaryGeneratedColumn()
  public id: number;

  @Column()
  public firstName: string;

  @Column()
  public lastName: string;

  @Column()
  public mail: string;

  @Column()
  public phone: string;

  @Column()
  public socialReason: string;

  @CreateDateColumn()
  public createdAt: Date;

  @UpdateDateColumn()
  public updatedAt: Date;

  @ManyToOne(() => AddressEntity, (address) => address.contacts)
  public address: AddressEntity;

  @OneToMany(() => QuoteEntity, (document) => document.client)
  public quote_client: QuoteEntity[];

  @OneToMany(() => QuoteEntity, (document) => document.company)
  public quote_company: QuoteEntity[];

  @OneToMany(() => InvoiceEntity, (document) => document.client)
  public invoice_client: InvoiceEntity[];

  @OneToMany(() => InvoiceEntity, (document) => document.company)
  public invoice_company: InvoiceEntity[];

  public constructor(data?: IContactEntity) {
    Object.assign(this, data);
  }
}
