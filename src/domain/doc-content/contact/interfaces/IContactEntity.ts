import { AddressEntity } from '../../address/entities/address.entity';

export interface IContactEntity {
  id?: number;
  firstName: string;
  lastName: string;
  socialReason: string;
  mail: string;
  phone: string;
  address: AddressEntity;
  createdAt: Date;
  updatedAt: Date;
}
