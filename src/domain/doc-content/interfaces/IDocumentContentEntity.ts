export interface IDocumentContentEntity {
  id?: number;
  comment: string;
  createdAt: Date;
  updatedAt: Date;
}
