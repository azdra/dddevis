import { Module } from '@nestjs/common';
import TypeormConfig from '../../configuration/typeorm.config';

const databaseProviders = [
  {
    provide: 'DATA_SOURCE',
    useFactory: async () => {
      return TypeormConfig.initialize();
    },
  },
];

@Module({
  providers: [...databaseProviders],
  exports: [...databaseProviders],
})
export class DatabaseModule {}
