import TypeormConfig from '../../../configuration/typeorm.config';
import { UpdateResult } from 'typeorm';
import ContactEntity from '../../../domain/doc-content/contact/entities/contact.entity';
import { IContactRepository } from '../../../domain/doc-content/contact/interfaceAdapters/repositories/IContactRepository';

export class ContactRepositoryImpl implements IContactRepository {
  public constructor(
    private repository = TypeormConfig.getRepository(ContactEntity),
  ) {}

  public createContact(invoice: ContactEntity): Promise<ContactEntity> {
    return this.repository.save(invoice);
  }

  public deleteContact(id: ContactEntity['id']): Promise<unknown> {
    return this.repository.delete(id);
  }

  public getContact(id: ContactEntity['id']): Promise<ContactEntity> {
    return this.repository.findOne({
      where: { id },
    });
  }

  public getContacts(): Promise<ContactEntity[]> {
    return this.repository.find();
  }

  public updateContact(
    id: ContactEntity['id'],
    invoice: ContactEntity,
  ): Promise<UpdateResult> {
    return this.repository.update(id, invoice);
  }
}
