import TypeormConfig from '../../../configuration/typeorm.config';
import InvoiceEntity from '../../../domain/invoice/entities/invoice.entity';
import { IInvoiceRepository } from '../../../domain/invoice/interfaceAdapters/repositories/IInvoiceRepository';

export class InvoiceRepositoryImpl implements IInvoiceRepository {
  constructor(
    private repository = TypeormConfig.getRepository(InvoiceEntity),
  ) {}

  public createInvoice(invoice: InvoiceEntity): Promise<InvoiceEntity> {
    return this.repository.save(invoice);
  }

  public deleteInvoice(id: InvoiceEntity['id']): Promise<unknown> {
    return this.repository.delete(id);
  }

  public getInvoice(id: InvoiceEntity['id']): Promise<InvoiceEntity> {
    return this.repository.findOneOrFail({
      where: { id },
      relations: ['company', 'client', 'lines'],
    });
  }

  public getInvoices(): Promise<InvoiceEntity[]> {
    return this.repository.find({ relations: ['company', 'client', 'lines'] });
  }

  public async updateInvoice(
    id: InvoiceEntity['id'],
    invoice: InvoiceEntity,
  ): Promise<InvoiceEntity> {
    await this.repository.update(id, invoice);
    return this.getInvoice(id);
  }
}
