import { QuoteEntity } from '../../../domain/quote/entities/quote.entity';
import TypeormConfig from '../../../configuration/typeorm.config';
import { IQuoteRepository } from '../../../domain/quote/interfaceAdapters/repositories/IQuoteRepository';

export class QuoteRepositoryImpl implements IQuoteRepository {
  constructor(private repository = TypeormConfig.getRepository(QuoteEntity)) {}

  public createQuote(quote: QuoteEntity): Promise<QuoteEntity> {
    console.log(quote);
    return this.repository.save(quote);
  }

  public deleteQuote(id: QuoteEntity['id']): Promise<unknown> {
    return this.repository.delete(id);
  }

  public getQuote(id: QuoteEntity['id']): Promise<QuoteEntity> {
    return this.repository.findOneOrFail({
      where: { id },
      relations: ['company', 'client', 'lines'],
    });
  }

  public getQuotes(): Promise<QuoteEntity[]> {
    return this.repository.find({ relations: ['company', 'client', 'lines'] });
  }

  public async updateQuote(
    id: QuoteEntity['id'],
    quote: QuoteEntity,
  ): Promise<QuoteEntity> {
    await this.repository.update(id, quote);
    return this.getQuote(id);
  }
}
