import TypeormConfig from '../../../configuration/typeorm.config';
import LineEntity from '../../../domain/doc-content/line/entities/line.entity';
import { ILineRepository } from '../../../domain/doc-content/line/interfaceAdapters/repositories/ILineRepository';

export class LineRepositoryImpl implements ILineRepository {
  public constructor(
    private repository = TypeormConfig.getRepository(LineEntity),
  ) {}

  public createLine(line: LineEntity): Promise<LineEntity> {
    return this.repository.save(line);
  }

  deleteLine(id: LineEntity['id']): Promise<unknown> {
    return this.repository.delete(id);
  }

  getLine(id: LineEntity['id']): Promise<LineEntity> {
    return this.repository.findOne({
      where: { id },
    });
  }

  getLines(): Promise<LineEntity[]> {
    return this.repository.find();
  }

  updateLine(id: LineEntity['id'], Line: LineEntity): Promise<LineEntity> {
    return this.repository.save({ ...Line, id });
  }
}
