import { Module } from '@nestjs/common';
import { InvoiceModule } from '../domain/invoice/invoice.module';
import { QuoteModule } from '../domain/quote/quote.module';

@Module({
  imports: [InvoiceModule, QuoteModule],
})
export class PresentersModule {}
