import CreateLineDto from '../line/create-line.dto';
import { IsNotEmpty, IsNumber, IsString } from 'class-validator';
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';

export default class CreateInvoiceDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  public comment: string;

  @ApiProperty()
  @IsNumber()
  public company: number;

  @ApiProperty()
  @IsNumber()
  public client: number;

  @ApiPropertyOptional({ type: CreateLineDto, isArray: true })
  public lines: CreateLineDto[];

  @ApiProperty()
  public paymentChoice: string;
}
