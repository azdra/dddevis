import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export default class CreateContactDto {
  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  public firstName: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  public lastName: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  public socialReason: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  public mail: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  public phone: string;
}
