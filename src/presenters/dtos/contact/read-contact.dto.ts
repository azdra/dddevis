import { ApiProperty } from '@nestjs/swagger';

export default class ReadContactDto {
  @ApiProperty()
  public id: number;

  @ApiProperty()
  public firstName: string;

  @ApiProperty()
  public lastName: string;

  @ApiProperty()
  public mail: string;

  @ApiProperty()
  public phone: string;

  @ApiProperty()
  public socialReason: string;

  @ApiProperty()
  public createdAt: Date;

  @ApiProperty()
  public updatedAt: Date;
}
