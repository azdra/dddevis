import { IsArray, IsNotEmpty, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import CreateLineDto from '../line/create-line.dto';
import CreateContactDto from '../contact/create-contact.dto';

export default class CreateQuoteDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  public comment: string;

  // @ApiProperty({ type: CreateContactDto })
  // public company: CreateContactDto;

  @ApiProperty({ type: CreateContactDto })
  public client: CreateContactDto;

  @ApiProperty({ type: CreateLineDto, isArray: true })
  @IsArray()
  @IsNotEmpty()
  public lines: CreateLineDto[];
}
