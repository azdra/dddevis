import CreateLineDto from '../line/create-line.dto';
import { IsNotEmpty, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import ReadContactDto from '../contact/read-contact.dto';

export default class ReadQuoteDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  public comment: string;

  // @ApiProperty()
  // public company: ReadContactDto;

  @ApiProperty()
  public client: ReadContactDto;

  @ApiProperty({ type: CreateLineDto, isArray: true })
  public lines: CreateLineDto[];
}
