import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsNumber, IsString } from 'class-validator';

export default class CreateLineDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  public description: string;

  @ApiProperty()
  @IsNumber()
  public unitPrice: number;

  @ApiProperty()
  @IsNumber()
  public quantity: number;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  public unit: string;

  @ApiProperty()
  @IsNumber()
  public tax: number;
}
