import { Controller, Get, Param, Res } from '@nestjs/common';
import { Response } from 'express';
import AbstractController from './abstract.controller';
import { ReadInvoiceUseCase } from '../../domain/invoice/use-cases/read-invoice.use-case';
import { ApiResponse, ApiTags } from '@nestjs/swagger';
import { ReadInvoicesUseCase } from '../../domain/invoice/use-cases/read-invoices.use-case';
import ReadInvoiceDto from '../dtos/invoice/read-invoice.dto';

@Controller('/invoice')
@ApiTags('Invoice')
export class InvoiceController extends AbstractController {
  constructor(
    private readInvoiceUseCase: ReadInvoiceUseCase,
    private readInvoicesUseCase: ReadInvoicesUseCase,
  ) {
    super();
  }

  @ApiResponse({
    status: 201,
    description: 'Get one quote',
    type: ReadInvoiceDto,
  })
  @Get('/:id')
  public readInvoice(@Res() response: Response, @Param('id') id: number) {
    return this.handleRequest(response, {
      service: this.readInvoiceUseCase,
      fn: 'execute',
      args: [id],
    });
  }

  @ApiResponse({
    status: 201,
    description: 'Get quote',
    type: ReadInvoiceDto,
    isArray: true,
  })
  @Get()
  public readInvoices(@Res() response: Response) {
    return this.handleRequest(response, {
      service: this.readInvoicesUseCase,
      fn: 'execute',
    });
  }
}
