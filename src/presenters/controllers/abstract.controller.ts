import { Response } from 'express';
import { IUseCase } from '../../infra/IUseCase';

export default class AbstractController {
  public async handleRequest(
    response: Response,
    args: {
      service: IUseCase;
      fn: string;
      args?: unknown[];
    },
  ) {
    const service = args['service'];
    const fn = args['fn'] ?? null;
    const fnArgs = args['args'] ?? [];

    if (!service && !fn) throw new Error('Aucun service demandé');

    try {
      const result = await service[fn](...fnArgs);
      response.status(200).json(result);
    } catch (e) {
      response.status(500).json({
        message: 'Une erreur est survenue',
        status: 500,
        error: e.message,
      });
    }
  }
}
