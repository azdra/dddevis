import { Body, Controller, Get, Param, Post, Put, Res } from '@nestjs/common';
import CreateQuoteDto from '../dtos/quote/create-quote.dto';
import AbstractController from './abstract.controller';
import { Response } from 'express';
import { CreateQuoteUseCase } from '../../domain/quote/use-cases/create-quote.use-case';
import { ReadQuoteUseCase } from '../../domain/quote/use-cases/read-quote.use-case';
import { TransformToInvoiceUseCase } from '../../domain/quote/use-cases/transform-to-invoice.use-case';
import { QuoteEntity } from '../../domain/quote/entities/quote.entity';
import { ApiResponse, ApiTags } from '@nestjs/swagger';
import { ReadQuotesUseCase } from '../../domain/quote/use-cases/read-quotes.use-case';
import ReadQuoteDto from '../dtos/quote/read-quote.dto';
import { AddLineToQuoteUseCase } from '../../domain/quote/use-cases/add-line-to-quote.use-case';
import CreateLineDto from '../dtos/line/create-line.dto';

@ApiTags('Quote')
@Controller('/quote')
export class QuoteController extends AbstractController {
  public constructor(
    private createQuoteUseCase: CreateQuoteUseCase,
    private readQuoteUseCase: ReadQuoteUseCase,
    private readQuotesUseCase: ReadQuotesUseCase,
    private addLineToQuoteUseCase: AddLineToQuoteUseCase,
    private transformToInvoiceUseCase: TransformToInvoiceUseCase,
  ) {
    super();
  }

  @Post()
  @ApiResponse({
    status: 201,
    description: 'Quote created',
    type: ReadQuoteDto,
  })
  public createQuote(
    @Res() response: Response,
    @Body() quoteDTO: CreateQuoteDto,
  ) {
    return this.handleRequest(response, {
      service: this.createQuoteUseCase,
      fn: 'execute',
      args: [quoteDTO],
    });
  }

  @ApiResponse({
    status: 201,
    description: 'Get quote',
    type: ReadQuoteDto,
  })
  @Post('transform-to-invoice/:id')
  public transformToInvoice(
    @Res() response: Response,
    @Param('id') quoteId: QuoteEntity['id'],
  ) {
    return this.handleRequest(response, {
      service: this.transformToInvoiceUseCase,
      fn: 'execute',
      args: [quoteId],
    });
  }

  @Put('add-line/:id')
  public addLineToQuote(
    @Res() response: Response,
    @Body() lineDto: CreateLineDto,
    @Param('id') quoteId: QuoteEntity['id'],
  ) {
    return this.handleRequest(response, {
      service: this.addLineToQuoteUseCase,
      fn: 'execute',
      args: [lineDto, quoteId],
    });
  }

  @ApiResponse({
    status: 201,
    description: 'Get quote',
    type: ReadQuoteDto,
  })
  @Get('/:id')
  public readQuote(
    @Res() response: Response,
    @Param('id') id: QuoteEntity['id'],
  ) {
    return this.handleRequest(response, {
      service: this.readQuoteUseCase,
      fn: 'execute',
      args: [id],
    });
  }

  @ApiResponse({
    status: 201,
    description: 'Get quote',
    type: ReadQuoteDto,
    isArray: true,
  })
  @Get()
  public readQuotes(@Res() response: Response) {
    return this.handleRequest(response, {
      service: this.readQuotesUseCase,
      fn: 'execute',
    });
  }
}
