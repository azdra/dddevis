start:
	@docker compose up

stop:
	@docker compose down

install:
	@docker compose run --rm app npm install

lint:
	@docker compose exec app npm run lint
	@docker compose exec app npm run format

enter:
	@docker compose exec app sh

test:
	@docker compose exec app npm run test

.PHONY: fixtures
fixtures:
	@docker compose exec app npm run fixtures